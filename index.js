const addTwoNumbers = (num1, num2) => {
    console.log(`Displayed sum of ${num1} and ${num2}`);
    console.log(num1 + num2);
};

addTwoNumbers(5, 15);

const subtractTwoNumbers = (num1, num2) => {
    console.log(`Displayed difference of ${num1} and ${num2}`);
    console.log(num1 - num2);
};

subtractTwoNumbers(20, 5);

const multiplyTwoNumbers = (num1, num2) => {
    let product = num1 * num2;
    console.log(`The product of ${num1} and ${num2}:`);
    console.log(product);
    return product;
};

multiplyTwoNumbers(50, 10);

const divideTwoNumbers = (num1, num2) => {
    let quotient = num1 / num2;
    console.log(`The quotient of ${num1} and ${num2}:`);
    console.log(quotient);
    return quotient;
};

divideTwoNumbers(50, 10);

const getAreaOfCircle = rad => {
    const circleArea = parseFloat((Math.pow(rad, 2) * Math.PI).toFixed(2));
    console.log(`The result of getting the area of a circle with ${rad} radius:`);
    console.log(circleArea);
    return circleArea;
};

getAreaOfCircle(15);

const getAverageOfNumbers = (...args) => {
    let averageVar = args.reduce((acc, sum) => acc + sum, 0) / args.length;
    let lastElement = args.pop();
    console.log(`The average of ${args.join(",")} and ${lastElement}`);
    console.log(averageVar);
    return averageVar;
};

getAverageOfNumbers(20, 40, 60, 80);


const isPassedTest = (score, totalScore) => {
    let percentage = (score / totalScore) * 100;
    let isPassed = percentage >= 75;
    console.log(`Is ${score}/${totalScore} a passing score?`);
    console.log(isPassed);
    return isPassed;
};

isPassedTest(38, 50);


